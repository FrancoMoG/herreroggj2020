﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System;

public class Anvil : MonoBehaviour
{
    public Sword _sword;
    public FireController fireController;
    public SpriteRenderer sprHammer;
    //public Image imgFillContainer;
    public Slider sliderFill;
    public Image imgCorrectBar;
    public Image imgPerfectBar;
    public TextMeshProUGUI txtRemainingHammers;


    [Range(.1f, .6f)]
    public float _fCorrectBarRange = 0f;
    [Range(.05f, .3f)]
    public float _fPerfectBarRange = 0f;

    private const float MAX_BOUNCE = 1;
    private const float MIN_BOUNCE = 0;

    private int _iNumberOfHammers = 0;

    private float _fTargetBounce = 1;
    private float _fBarProgress = .0f;
    private float _fStartPosition = .0f;
    private float _fBounceSpeed = 2f;
    private float v3HammerStartScale;

    private Vector3 v3HammerStartPosition;

    private bool _bBarBouncing = false;
    private bool _bIsHammering = false;

    private Transform _tCam;
    private Transform _tHammer;

    private Color32 _clrCorrectBar;
    private Color32 _clrPerfectBar;
    private readonly Color32 HAMMER_PERFECT = Color.green;
    private readonly Color32 HAMMER_CORRECT = Color.yellow;
    private readonly Color32 HAMMER_MISS = Color.red;

    private float _fCorrectBarStartHeight;
    private float _fPerfectBarStartHeight;

    private const int MAX_NUMBER_OF_HAMMERS = 5;
    private const float PERFECT_FIRE_VALUE = .7f;

    private readonly Vector2 PERFECT_VALUE_RANGE = new Vector2(.0f, .15f);
    private readonly Vector2 CORRECT_VALUE_RANGE = new Vector2(.1f, .3f);

    private object _tweenHammer = new object();

    private GameManager _gm;

    private int _currentLevel = 1;

    // Start is called before the first frame update
    void Start()
    {
        _gm = FindObjectOfType<GameManager>();
        _gm.OnLevelSet += SetLevel;
        _tCam = GameObject.FindGameObjectWithTag("MainCamera").transform;
        RestartAnvil();
        _tHammer = sprHammer.transform;
        v3HammerStartPosition = _tHammer.position;
        v3HammerStartScale = _tHammer.localScale.x;

        var rtCorrectBar = imgCorrectBar.transform as RectTransform;
        _fCorrectBarStartHeight = rtCorrectBar.sizeDelta.y;

        var rtPerfectBar = imgPerfectBar.transform as RectTransform;
        _fPerfectBarStartHeight = rtPerfectBar.sizeDelta.y;


        //SET STARTING FILL AMOUNT
        sliderFill.value = _fBarProgress;
        //PREPARE BAR RANGE
        PrepareCorrectBarRange();
        //PRESERVE STARTING COLOR
        _clrCorrectBar = imgCorrectBar.color;
        _clrPerfectBar = imgPerfectBar.color;
    }

    private void SetLevel(LevelData levelData)
    {
        RestartAnvil();
        _currentLevel = levelData.LevelNumber;
        //Set Min and max and factor
        //_fBounceSpeed = Mathf.Min(2, Mathf.Max(.75f, _currentLevel * .2f));
        _fBounceSpeed = 0.4f + (_currentLevel-1) * .14f;
        if(_currentLevel==10) _fBounceSpeed = 1.0f;
    }

    public void RestartAnvil()
    {
        SetUIEnable(true);
        sprHammer.enabled = true;
        _iNumberOfHammers = 0;
        RefreshNumberOfHammersUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (_bBarBouncing)
        {
            AnvilFillContainerBounce();
        }
        else
        {
        }
        // Change bars heights depending on _fCorrectBarRange and _fPerfectBarRange
        _fCorrectBarRange = Mathf.Max(CORRECT_VALUE_RANGE.x, CORRECT_VALUE_RANGE.y - Mathf.Abs(fireController.GetFireValue() - PERFECT_FIRE_VALUE)*.3f);
        _fPerfectBarRange = Mathf.Max(PERFECT_VALUE_RANGE.x, PERFECT_VALUE_RANGE.y - Mathf.Abs(fireController.GetFireValue() - PERFECT_FIRE_VALUE)*.3f);
        SetBarsHeight();
    }

    private void RefreshNumberOfHammersUI()
    {
        txtRemainingHammers.text = (MAX_NUMBER_OF_HAMMERS - _iNumberOfHammers).ToString() + '/' + MAX_NUMBER_OF_HAMMERS.ToString();
    }

    void OnMouseDown()
    {
        if (_iNumberOfHammers < MAX_NUMBER_OF_HAMMERS)
        {
            if (!_bBarBouncing)
            {
                SoundManager.Instance.PlayEffort();
                _bBarBouncing = true;
                DOTween.Sequence()
                .Append(_tHammer.DOMove(_tHammer.position + new Vector3(1, 1, 0) * 2, .1f).SetEase(Ease.Linear))
                .Join(_tHammer.DOScale(_tHammer.localScale + Vector3.one * .4f, .1f))
                .SetId(_tweenHammer);
            }

        }

    }

    void OnMouseUp()
    {
        if (_iNumberOfHammers < MAX_NUMBER_OF_HAMMERS)
        {
            if (_bBarBouncing)
            {
                if (!_bIsHammering)
                {
                    Hammer();
                }
            }
        }
    }


    private void SetUIEnable(bool bEnable)
    {
        imgCorrectBar.enabled = imgPerfectBar.enabled = sliderFill.enabled = bEnable;
    }

    private void PrepareCorrectBarRange()
    {

        SetBarsHeight();
        GenerateRandomBarsPosition();
    }


    private void AnvilFillContainerBounce()
    {
        _fBarProgress = Mathf.MoveTowards(_fBarProgress, _fTargetBounce, Time.deltaTime * _fBounceSpeed);

        sliderFill.value = _fBarProgress;

        if (HUtils.LerpAtTarget(_fBarProgress, _fTargetBounce, .01f))
        {
            _fTargetBounce = (_fTargetBounce == MAX_BOUNCE) ? MIN_BOUNCE : MAX_BOUNCE;
        }
    }

    private IEnumerator SplashFX()
    {
        sprHammer.GetComponentsInChildren<SpriteRenderer>()[1].flipX = HUtils.RandBool();
        sprHammer.GetComponentsInChildren<SpriteRenderer>()[1].enabled = true;

        var emitParams = new ParticleSystem.EmitParams();
        this.GetComponentInChildren<ParticleSystem>().Emit(emitParams, 20);

        yield return new WaitForSeconds(.15f);
        sprHammer.GetComponentsInChildren<SpriteRenderer>()[1].enabled = false;
    }

    private void Hammer()
    {
        _bIsHammering = true;


        DOTween.Sequence()
            .Append(_tHammer.DOMove(v3HammerStartPosition, .05f).SetEase(Ease.Linear))
            .Join(_tHammer.DOScale(v3HammerStartScale, .05f))
            .OnComplete(() => { StartCoroutine(SplashFX()); })
            .SetId(_tweenHammer);

        //DOTween.Kill(o);

        /*
        Debug.Log("CURRENT VALUE: " + _fBarProgress);
        Debug.Log("START POSITION: " + _fStartPosition);
        Debug.Log("PERFECT MIN VALUE: " + (_fPerfectBarRange + _fStartPosition - _fPerfectBarRange / 2));
        Debug.Log("PERFECT MAX VALUE: " + (_fPerfectBarRange + _fStartPosition + _fPerfectBarRange / 2));
        */
        _iNumberOfHammers++;

        float shakeIntensity = .1f;

        var amountRepaired = _sword.OriginalBreakAmount / 5f;
        Debug.Log("EEEEEEEEEEEEEEEEE");
        SoundManager.Instance.PlayEffortDown();
        if (HUtils.LerpAtTarget(_fBarProgress, _fStartPosition, _fPerfectBarRange / 2))
        {
            shakeIntensity = .4f;
            SoundManager.Instance.PlayGoodHit();
            //imgCorrectBar.color = HAMMER_PERFECT;
            Debug.Log("Good");
        }
        else if (HUtils.LerpAtTarget(_fBarProgress, _fStartPosition, _fCorrectBarRange / 2))
        {
            amountRepaired *= .5f;
            shakeIntensity = .2f;

            //imgCorrectBar.color = HAMMER_CORRECT;
            if (amountRepaired < 10)
            {
                SoundManager.Instance.PlayHotHit();
                Debug.Log("Hot");
            }
            else
            {
                SoundManager.Instance.PlayColdHit();
                Debug.Log("Cold");
            }
        }
        else
        {
            amountRepaired = 0;
            Debug.Log("Miss");
            SoundManager.Instance.PlayMissedHit();
            //imgCorrectBar.color = HAMMER_MISS;

        }
        DOTween.Sequence().Append(_tCam.DOShakePosition(.1f, shakeIntensity));

        _sword.SetBreakLevel(_sword.CurrentBreakLevel - amountRepaired);

        // CALL FUNCTION TO GET SWORD SPRITE
        RefreshNumberOfHammersUI();
        _bBarBouncing = false;

        StartCoroutine(ResumeBouncing(1));


    }

    private IEnumerator ResumeBouncing(float delay = 1.0f)
    {
        yield return new WaitForSeconds(delay);

        // CALL FUNCTION TO GET SWORD SPRITE
        if (_iNumberOfHammers < MAX_NUMBER_OF_HAMMERS)
        {
            //RESET POSITION
            GenerateRandomBarsPosition();

            //imgCorrectBar.color = _clrCorrectBar;
            //sliderFill.value = 0;
            //_fBarProgress = 0;
        }
        else
        {
            SetUIEnable(false);
            _gm.NotifyRepairDone(_sword);

        }
        _bIsHammering = false;

    }

    private void SetBarsHeight()
    {
        //SETS THE CORRECT HEIGHT
        var rtCorrectBar = imgCorrectBar.transform as RectTransform;
        rtCorrectBar.sizeDelta = new Vector2(rtCorrectBar.sizeDelta.x, _fCorrectBarStartHeight * _fCorrectBarRange);

        var rtPerfectBar = imgPerfectBar.transform as RectTransform;
        rtPerfectBar.sizeDelta = new Vector2(rtPerfectBar.sizeDelta.x, _fPerfectBarStartHeight * _fPerfectBarRange);
    }

    private void GenerateRandomBarsPosition()
    {
        //SETS A RANDOM POSITION
        _fStartPosition = UnityEngine.Random.Range(_fCorrectBarRange / 2, 1 - _fCorrectBarRange / 2);

        imgCorrectBar.transform.localPosition = new Vector2(imgCorrectBar.transform.localPosition.x,
             _fStartPosition * sliderFill.GetComponent<RectTransform>().rect.height - sliderFill.GetComponent<RectTransform>().rect.height / 2);

        imgPerfectBar.transform.localPosition = new Vector2(imgPerfectBar.transform.localPosition.x,
             _fStartPosition * sliderFill.GetComponent<RectTransform>().rect.height - sliderFill.GetComponent<RectTransform>().rect.height / 2);
    }

    public bool IsHammerUp()
    {
        return _bBarBouncing;
    }

}
