﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RandomShake(float fIntensity)
    {
        StartCoroutine(RandomShakeXY(fIntensity));
    }

    IEnumerator RandomShakeXY(float fIntensity)
    {
        bool bPositiveX = HUtils.RandBool();
        bool bPositiveY = HUtils.RandBool();

        Vector3 CAMERA_X_SHAKE = new Vector3(0.02f, 0f, 0f);
        Vector3 CAMERA_Y_SHAKE = new Vector3(0f, 0.02f, 0f);

        float EARTHQUAKE_ANIMATION_SPEED = .0125f;


        this.transform.position += CAMERA_X_SHAKE * fIntensity * GetPositive(bPositiveX);
        yield return new WaitForSeconds(EARTHQUAKE_ANIMATION_SPEED);

        this.transform.position += CAMERA_Y_SHAKE * fIntensity * GetPositive(bPositiveY);
        yield return new WaitForSeconds(EARTHQUAKE_ANIMATION_SPEED);

        this.transform.position += CAMERA_X_SHAKE * fIntensity * -GetPositive(bPositiveX);
        yield return new WaitForSeconds(EARTHQUAKE_ANIMATION_SPEED);

        this.transform.position += CAMERA_Y_SHAKE * fIntensity * -GetPositive(bPositiveY);
        yield return new WaitForSeconds(EARTHQUAKE_ANIMATION_SPEED);

    }

    private int GetPositive(bool bPositive)
    {
        return (bPositive) ? 1 : -1;
    }
}
