﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsController : MonoBehaviour
{
    public Text coinsText;
    private readonly int _coinsNumber = 0;

    // Start is called before the first frame update
    void Start()
    {
        SetCoins(_coinsNumber);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetCoins(int coinsNumber)
    {
        coinsText.text =  coinsNumber.ToString();
    }
}
