﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CreditsController : MonoBehaviour
{
    // Start is called before the first frame update
    public MainMenu MainMenu;
    public CanvasGroup CanvasGroup;

    private void Awake()
    {
        CanvasGroup.alpha = 0;
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        DOTween.Kill(CanvasGroup);
        CanvasGroup
            .DOFade(1, 0.3f)
            .OnComplete(() => CanvasGroup.interactable = true);
    }

    public void GoBackToMainMenu()
    {
        CanvasGroup.interactable = false;
        DOTween.Kill(CanvasGroup);
        MainMenu.Show();
        CanvasGroup
            .DOFade(0, 0.3f);
    }
}
