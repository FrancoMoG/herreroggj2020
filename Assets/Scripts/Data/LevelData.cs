﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[Serializable]
public class SwordSprites
{
    public Sprite Color;
    public Sprite Mask;
}

[CreateAssetMenu(menuName = "Level/Level Asset")]
public class LevelData : ScriptableObject
{
    public bool IsBoss;
    public int LevelNumber;

    [Header("Sword")]
    [SerializeField] private List<SwordSprites> _breakLevelSprites;
    [Range(0, 100)] public float BreakAmount;

    [Header("Client")]
    public AudioClip ClientVoice;
    public AudioClip ClientVoiceAngry;
    public AudioClip ClientVoiceHappy;
    public Sprite Client;
    public Sprite Request;

    [Header("Result")]
    public RuntimeAnimatorController _happyAnim;
    public RuntimeAnimatorController _sadAnim;
    public Sprite _happy;
    public Sprite _sad;
    public Sprite ClientResult;

    [Header("Reward")]
    public int Reward;

    public SwordSprites GetSpriteForBrokenLevel(float brokenLevelBetweenZeroAndHundred)
    {
        Assert.IsTrue(_breakLevelSprites.Count == 4);

        brokenLevelBetweenZeroAndHundred = Mathf.Clamp(brokenLevelBetweenZeroAndHundred, 0, 100f);

        if (brokenLevelBetweenZeroAndHundred <= 10)
        {
            return _breakLevelSprites[0];
        }

        if (brokenLevelBetweenZeroAndHundred <= 40)
        {
            return _breakLevelSprites[1];
        }

        if (brokenLevelBetweenZeroAndHundred <= 70)
        {
            return _breakLevelSprites[2];
        }

        return _breakLevelSprites[3];
    }

    public Sprite GetHappinessSprite(ClientHappiness clientHappiness)
    {
        if (clientHappiness == ClientHappiness.Happy) return _happy;
        return _sad;
    }

    public RuntimeAnimatorController GetHappinessAnim(ClientHappiness clientHappiness)
    {
        if (clientHappiness == ClientHappiness.Happy) return _happyAnim;
        return _sadAnim;
    }
}
