﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndScreen : MonoBehaviour
{
    [SerializeField] private CanvasGroup _cg;
    [SerializeField] private Button _replay;
    [SerializeField] private List<GameObject> _happy;
    [SerializeField] private List<GameObject> _unhappy;

    private void Awake()
    {
        FindObjectOfType<GameManager>().OnGameEnded += ShowEndScreen;
        _cg.alpha = 0f;
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        _replay.onClick.AddListener(Replay);
    }

    private void OnDisable()
    {
        _replay.onClick.RemoveListener(Replay);
    }

    private void Replay()
    {
        SoundManager.Instance.PlayMainTheme();
        SoundManager.Instance.IntroMusicAudioSource.volume = 1;
        SceneManager.LoadScene(0);
    }

    private void ShowEndScreen(bool isBossHappy)
    {
        SoundManager.Instance.PlayEndScreenMusic();
        gameObject.SetActive(true);
        _cg.blocksRaycasts = true;
        _cg.interactable = false;

        _happy.ForEach(a => a.SetActive(isBossHappy));
        _unhappy.ForEach(a => a.SetActive(!isBossHappy));

        _cg
            .DOFade(1f, 2f)
            .OnComplete(() => 
            {
                print("Game Ended");
                _cg.interactable = true;
            });
    }   
}
