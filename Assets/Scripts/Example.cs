﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example : MonoBehaviour
{
    [SerializeField] LevelData _level;
    [SerializeField] RepairRequest _repairRequest;
    [SerializeField] Sword _sword;
    [SerializeField] [Range(0, 100)] float _breakLevel;
    [SerializeField] [Range(0, 1f)] float _temperature;


    private void OnValidate()
    {
        //_repairRequest.SetLevel(_level);

        _sword.SetLevel(_level);
        _sword.SetBreakLevel(_breakLevel);
        _sword.SetTemperature(_temperature);
    }
}
