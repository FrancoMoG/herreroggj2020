﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FireController : MonoBehaviour
{
    [SerializeField] Sword _sword;
    private float _fireValue = 0;
    //private Animator _fireAnimator;
    //private Vector2 _initialSwordPosition;

    private bool _bForging = false;

    private Vector3 v3ForgePosition = new Vector3(-5, -8.36f, 0);
    private Vector3 v3AnvilPosition = new Vector3(0, -8.36f, 0);

    private Vector3 v3ForgeRotation = new Vector3(0, 0, 110);
    private Vector3 v3AnvilRotation = new Vector3(0, 0, 60);

    private float _fAnimationSpeed = 15;

    private Vector3 v3TargetPos;
    private Quaternion qTargetRot;

    private bool _bOnRepairScreen = false;

    private float _fHeatUpRatio = .25f;
    private float _fCooldownRatio = .06f;

    public Anvil anvil;

    private bool _hasPlayedIntenseFire = false;

    void Awake()
    {
        v3TargetPos = v3AnvilPosition;
        qTargetRot = Quaternion.Euler(v3AnvilRotation);
        var gm = FindObjectOfType<GameManager>();
        gm.OnRepairStart += OnRepairStart;
        gm.OnRepairDone += OnRepairDone;
        gm.OnLevelSet += OnLevelSet;
    }

    private void OnLevelSet(LevelData obj)
    {
        _fireValue = 0;
    }

    private void OnRepairDone(Result obj)
    {
        _bOnRepairScreen = false;
    }

    private void OnRepairStart(LevelData obj)
    {
        _bOnRepairScreen = true;

    }

    // Start is called before the first frame update
    void Start()
    {
        //_fireAnimator = GetComponent<Animator>();
        //_initialSwordPosition = _sword.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (_bOnRepairScreen)
        {
            //handleMouseClickEvent();
           // _fireAnimator.SetFloat("FireValue", _fireValue);
            // Set sword temprature which increases redness of sword
            _sword.SetTemperature(_fireValue);
            // Move sword closer/further from fire
            _sword.transform.localPosition = Vector2.Lerp(_sword.transform.localPosition, v3TargetPos, Time.deltaTime * _fAnimationSpeed);
            _sword.transform.localRotation = Quaternion.Lerp(_sword.transform.localRotation, qTargetRot, Time.deltaTime * _fAnimationSpeed);
            if (_bForging)
            {
                if (HUtils.LerpAtTarget(_sword.transform.localPosition, v3TargetPos, 0.5f) && !_hasPlayedIntenseFire)
                {
                    _hasPlayedIntenseFire = true;
                    SoundManager.Instance.PlayFireIntenseLoop();
                }
                _fireValue += _fHeatUpRatio * Time.deltaTime;
            }
            else
            {
                _fireValue -= _fCooldownRatio * Time.deltaTime;
            }

            _fireValue = Mathf.Min(1, Mathf.Max(0, _fireValue));

        }
    }

    // Increase/Decrease fire heat
    private void handleMouseClickEvent()
    {

        if (Input.GetMouseButton(0) && _fireValue < 1)
        {
            _fireValue += 0.2f * Time.deltaTime * 1.5f;
        }
        else if (_fireValue >= 0)
        {
            _fireValue -= 0.1f * Time.deltaTime;
        }
    }

    // Increase/Decrease fire heat
    private void OnMouseDrag()
    {
        if (!anvil.IsHammerUp())
        {
            _bForging = true;
            v3TargetPos = v3ForgePosition;
            qTargetRot = Quaternion.Euler(v3ForgeRotation);
        }

    }

    void OnMouseUp()
    {
        _hasPlayedIntenseFire = false;
        SoundManager.Instance.StopEffect();
        SoundManager.Instance.PlayFireLoop();
        _bForging = false;
        v3TargetPos = v3AnvilPosition;
        qTargetRot = Quaternion.Euler(v3AnvilRotation);
    }

    public float GetFireValue()
    {
        return _fireValue;
    }
}
