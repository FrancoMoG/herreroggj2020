﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;


public class ForgeGlow : MonoBehaviour
{
    private SpriteRenderer _sprGlow;
    public float fAnimSpeed = 2;
    // Start is called before the first frame update
    void Awake()
    {
        _sprGlow = GetComponent<SpriteRenderer>();
        StartCoroutine(RandomDelay());
    }

    private IEnumerator RandomDelay()
    {
        float fDealy = Random.Range(0, 3);
        yield return new WaitForSeconds(fDealy);
        DOTween.Sequence()
                .Append(_sprGlow.DOFade(0, fAnimSpeed).SetEase(Ease.Linear)).SetLoops(-1, LoopType.Yoyo);// Append(_sprGlow.DOFade(1, 1f).SetEase(Ease.Linear)).SetLoops(4, LoopType.Restart);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
