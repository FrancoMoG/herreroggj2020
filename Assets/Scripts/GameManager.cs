﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum ClientHappiness
{
    Neutral,
    Sad,
    Happy
}

public struct Result
{
    public LevelData LevelData;
    public ClientHappiness ClientHappiness;
    public float GainedCoins;
    public bool IsLastLevel;
}

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<LevelData> _levels;

    private int _currentLevel;
    public LevelData CurrentLevel => _levels[_currentLevel];

    public int Coins => _coins;

    public event Action<Result> OnRepairDone;
    public event Action<LevelData> OnLevelSet;
    public event Action<LevelData> OnRepairStart;
    public event Action<bool> OnGameEnded;

    // State
    private int _coins;

    private void Start()
    {
        // Uncomment if we want to sort by LevelNumber defined in the data
        _levels = _levels.OrderBy(l => l.LevelNumber).ToList();
        _coins = 0;
        StartLevel(0);
    }

    private void Update()
    {
#if !UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
#endif
    }

    public void StartLevel(int index)
    {
        _currentLevel = index;
        OnLevelSet?.Invoke(CurrentLevel);
    }

    public void StartRepair()
    {
        OnRepairStart?.Invoke(CurrentLevel);
    }

    public void ShowNextLevel()
    {
        if(_currentLevel + 1 >= _levels.Count)
        {
            // SHOULD NEVER HAPPEN, ALWAYS CALLS ShowBossHappy
            return;
        }

        StartLevel(_currentLevel + 1);
    }

    public void NotifyRepairDone(Sword sword)
    {
        var coins = sword.OriginalBreakAmount - sword.CurrentBreakLevel;
        _coins = (int)coins;
        var clientHappiness = sword.CurrentBreakLevel <= 10 ? ClientHappiness.Happy : ClientHappiness.Sad;
        var result = new Result()
        {
            LevelData = CurrentLevel,
            ClientHappiness = clientHappiness,
            GainedCoins = coins,
            IsLastLevel = _currentLevel + 1 >= _levels.Count
        };

        OnRepairDone?.Invoke(result);
    }

    public void ShowBossHappy(bool isBossHappy)
    {
        // End game
        OnGameEnded?.Invoke(isBossHappy);
        return;
    }
}
