﻿#pragma warning disable 618
#if UNITY_EDITOR
#define DEBUGGING
#endif
using System;
using System.Collections.Generic;
using UnityEngine;

public static class HUtils
{
    public static T NextEnum<T>(this T src)
    {
        //if (!typeof(T).IsEnum) throw new ArgumentException(String.Format("Argument {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }

    /// <summary>
    /// Returns a random integer between min and max value.
    /// </summary>
    public static float RandFloat(float min, float max)
    {
        return UnityEngine.Random.Range(min, max);
    }
    /// <summary>
    /// Returns a random integer between min and max value.
    /// </summary>
    public static int RandInt(int min, int max)
    {
        return (int)UnityEngine.Random.Range(min, max + 1);
    }
    /// <summary>
    /// Returns a random integer from 0 to array index -1
    /// </summary>
    public static int RandArrayIndex<T>(T[] array)
    {
        return RandInt(0, array.Length - 1);
    }
    /// <summary>
    /// Returns a random integer from 0 to List index -1
    /// </summary>
    public static int RandListIndex<T>(List<T> array)
    {
        return RandInt(0, array.Count - 1);
    }
    /// <summary>
    /// Returns a random element from an array
    /// </summary>
    public static T RandElement<T>(T[] array)
    {
        return array[RandInt(0, array.Length - 1)];
    }

    /// <summary>
    /// Returns a random bool.
    /// </summary>
    public static bool RandBool()
    {
        return RandInt(0, 1) == 1;
    }

    public static void SetAlpha(UnityEngine.UI.Image img, float alpha)
    {
        img.color = new Color(img.color.r, img.color.g, img.color.b, alpha);

    }

    public static void SetAlpha(UnityEngine.SpriteRenderer spr, float alpha)
    {
        spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, alpha);

    }

    public static void SetAlpha(UnityEngine.UI.Text txt, float alpha)
    {
        txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, alpha);

    }

    public static void SetAlpha(TextMesh txt, float alpha)
    {
        txt.color = new Color(txt.color.r, txt.color.g, txt.color.b, alpha);

    }

    public static void SetParticleSysEmissionRate(ParticleSystem particleSystem, float emissionRate)
    {
        var emission = particleSystem.emission;
        var rate = emission.rate;
        rate.constantMax = emissionRate;
        emission.rate = rate;
    }
    public static void StopParticleSysEmission(ParticleSystem particleSystem)
    {
        SetParticleSysEmissionRate(particleSystem, 0);
    }
    public static int EnumSize<T>(T enumVar)
    {
        return System.Enum.GetValues(typeof(T)).Length;
    }

    public static bool VectorAtTarget(Vector3 vectorStart, Vector3 vectorFinish, float distanceThreshold)
    {
        return Vector3.Distance(vectorStart, vectorFinish) <= distanceThreshold;
    }


    public static bool LerpAtTarget(Vector3 v3CurrentPosition, Vector3 v3Target, float fTolerance)
    {
        return Vector3.Distance(v3CurrentPosition, v3Target) <= fTolerance;
    }

    public static bool LerpAtTarget(float fCurrentPosition, float fTarget, float fTolerance)
    {
        return Mathf.Abs(fCurrentPosition - fTarget) <= fTolerance;
    }

    public static bool SignedLerpAtTarget(float fCurrentPosition, float fTarget, float fTolerance)
    {
        return fCurrentPosition - fTarget <= fTolerance;
    }

    public static Sprite LoadSpriteResource(string sSprite)
    {
        return Resources.Load<Sprite>(sSprite);
    }

    public static bool Is(Transform t, string tag)
    {
        return t.tag.Equals(tag);
    }

  
    /// <summary>
    /// Returns a string representation of a float value, without its decimal part.
    /// </summary>
    public static string FloatToString(float f)
    {
        return ((int)f).ToString("F0");
    }
    /// <summary>
    /// Returns screen size in a Vector2(width, height).
    /// </summary>
    public static Vector2 GetScreenDimentions()
    {
        return new Vector2(Screen.width, Screen.height);
    }

    public static DateTime FromUnixTime(long unixTime)
    {
        var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return epoch.AddSeconds(unixTime);
    }

    public static long ToUnixTime(DateTime date)
    {
        return Convert.ToInt64(ToUnixTimeDouble(date));
    }

    public static double ToUnixTimeDouble(DateTime date)
    {
        var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        return (date - epoch).TotalSeconds;
    }

    /// <summary>
    /// Returns current time IN SECONDS.
    /// </summary>
    public static double GetCurrentTimeDouble()
    {
        // Este metodo debe leer del server un timestamp de la hora actual (unix)
        // *** IMPLEMENTAR ****
        return ToUnixTimeDouble(DateTime.Now.ToUniversalTime());// DateTime.Now.ToUnixTimeDouble();
    }
    /// <summary>
    /// Returns current time IN SECONDS.
    /// </summary>
    public static long GetCurrentTime()
    {
        // Este metodo debe leer del server un timestamp de la hora actual (unix)
        // *** IMPLEMENTAR ****
        return ToUnixTime(DateTime.Now.ToUniversalTime());// DateTime.Now.ToUnixTimeDouble();
    }
    /// <summary>
    /// Returns current time IN MILLISECONDS.
    /// </summary>
    public static long GetCurrentTimeInMilliseconds()
    {
        return GetCurrentTime() * 1000;
    }

    public static T FindComponentInChildWithTag<T>(GameObject parent, string tag) where T : Component
    {
        Transform t = parent.transform;
        foreach (Transform tr in t)
        {
            if (tr.tag == tag)
            {
                return tr.GetComponent<T>();
            }
        }
        return null;
    }

    public static Vector2 GetScreenToWorldPosition(GameObject WorldObject)
    {
        //first you need the RectTransform component of your canvas
        RectTransform CanvasRect = GameObject.Find("Canvas").GetComponent<Canvas>().GetComponent<RectTransform>();

        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(WorldObject.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        return WorldObject_ScreenPosition;

    }
}
