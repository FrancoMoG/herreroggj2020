﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MainMenu : MonoBehaviour
{
    public CreditsController Credits;
    public CanvasGroup CanvasGroup;

    RectTransform _credits;

    public void Show()
    {
        CanvasGroup.interactable = false;
        DOTween.Kill(CanvasGroup);
        CanvasGroup
            .DOFade(1, 0.3f)
            .OnComplete(() => CanvasGroup.interactable = true);
    }

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Debug.Log("e");
        Application.Quit();
    }

    public void ShowCredits()
    {
        CanvasGroup.interactable = false;
        DOTween.Kill(CanvasGroup);
        Credits.Show();
        CanvasGroup
            .DOFade(0, 0.3f);
    }
}
