﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class RepairRequest : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _client;
    [SerializeField] private SpriteRenderer _request;
    [SerializeField] private Button _repair;
    [SerializeField] private Button _backUp;
    [SerializeField] private TextMeshProUGUI _coins;

    public SpriteRenderer sprBackBoss;

    private RectTransform _rectTransform;
    private Camera _cam;
    private GameManager _gm;

    private object _oTweeenYoyo = new object();

    private Transform tClientDialog;

    private Vector3 v3StartScale, v3StartPos;


    private void Awake()
    {
        _cam = Camera.main;
        _gm = FindObjectOfType<GameManager>();
        _gm.OnLevelSet += SetLevel;
        _gm.OnRepairDone += OnRepairDone;
        _rectTransform = transform as RectTransform;
        tClientDialog = _client.GetComponentsInChildren<SpriteRenderer>()[1].transform;

        v3StartScale = tClientDialog.transform.localScale;
        v3StartPos = tClientDialog.transform.localPosition;
        SoundManager.Instance.IntroMusicAudioSource.volume = 0.3f;
    }

    private void OnRepairDone(Result result)
    {
        gameObject.SetActive(false);
        _repair.interactable = false;
        _rectTransform.offsetMax = new Vector2(_rectTransform.offsetMin.x, 0);
        _rectTransform.offsetMin = new Vector2(_rectTransform.offsetMin.x, 0);


    }

    private void OnEnable()
    {
        _repair.onClick.AddListener(Repair);
        _backUp.onClick.AddListener(Back);
    }

    private void OnDisable()
    {
        _repair.onClick.RemoveListener(Repair);
        _backUp.onClick.RemoveListener(Back);
    }

    private void Repair()
    {
        AnimateUp();
    }

    public void AnimateUp()
    {
        _repair.interactable = false;
        _backUp.interactable = false;

        DOVirtual.Float(0, -10f, 1f, val =>
        {
            _cam.transform.position = new Vector3(_cam.transform.position.x, val, _cam.transform.position.z);
        })
        .SetEase(Ease.InOutCubic);

        DOVirtual.Float(0, _rectTransform.rect.height, 1f, val =>
        {
            _rectTransform.offsetMax = new Vector2(_rectTransform.offsetMax.x, val);
            _rectTransform.offsetMin = new Vector2(_rectTransform.offsetMin.x, val);
        })
        .SetEase(Ease.InOutCubic)
        .OnComplete(() =>
        {
            SoundManager.Instance.PlayFireLoop();
            _gm.StartRepair();
            _backUp.interactable = true;
        });
    }

    public void Back()
    {
        SoundManager.Instance.StopEffect();
        _repair.interactable = false;
        _backUp.interactable = false;

        DOVirtual.Float(-10f, 0, 1f, val =>
        {
            _cam.transform.position = new Vector3(_cam.transform.position.x, val, _cam.transform.position.z);
        })
        .SetEase(Ease.InOutCubic);

        DOVirtual.Float(_rectTransform.rect.height, 0, 1f, val =>
        {
            _rectTransform.offsetMax = new Vector2(_rectTransform.offsetMin.x, val);
            _rectTransform.offsetMin = new Vector2(_rectTransform.offsetMin.x, val);
        })
        .SetEase(Ease.InOutCubic)
        .OnComplete(() =>
        {
            _repair.interactable = true;
        });
    }

    public void SetLevel(LevelData levelData)
    {
        tClientDialog.transform.localPosition = v3StartPos;
        tClientDialog.transform.localScale = v3StartScale;

        /*
        DOTween.Sequence()
          .Append(tClientDialog.DOMove(v3StartPos, .1f).SetEase(Ease.Linear))
          .Join(tClientDialog.DOScale(v3StartScale, .1f))
          .OnComplete(() =>
          {
              _repair.interactable = true;
          });
        */
        if (levelData.IsBoss)
        {
            _client.transform.localScale *= .8f;
            SoundManager.Instance.StartBossTheme();
        }

        tClientDialog.GetComponentInChildren<Animator>().enabled = false;

        _coins.text = $"{_gm.Coins}";
        gameObject.SetActive(true);
        _client.sprite = levelData.Client;
        _request.sprite = levelData.Request;

        SoundManager.Instance.PlaySteps();

        _client.transform.DOMoveY(.3f, .1f)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Yoyo)
            .SetId(_oTweeenYoyo);

        _client.transform.DOMoveX(-2, 2).SetEase(Ease.Linear).OnComplete(() =>
        {
            SoundManager.Instance.StopEffect();
            SoundManager.Instance.PlayEffect(levelData.ClientVoice);

            DOTween.Kill(_oTweeenYoyo);
            if (levelData.IsBoss)
            {
                Debug.Log("MONSTER");
                _client.sprite = levelData.ClientResult;
                sprBackBoss.DOFade(1, 1f);
            }
            DOTween.Sequence()
           .Append(tClientDialog.DOMove(v3StartPos + new Vector3(.8f, -.28f, 0), .1f).SetEase(Ease.Linear))
           .Join(tClientDialog.DOScale(new Vector3(.5f, .5f, 0), .1f))
           .OnComplete(() =>
           {
               /* SET ACTIVE BUTTON REPAIR */
               _repair.interactable = true;
           });

        });
    }
}
