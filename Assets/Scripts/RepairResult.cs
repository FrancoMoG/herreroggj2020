﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class RepairResult: MonoBehaviour
{
    [SerializeField] private SpriteRenderer _client;
    [SerializeField] private SpriteRenderer _resultHappiness;
    [SerializeField] private TextMeshProUGUI _coins;

    private RectTransform _rectTransform;
    private GameManager _gm;
    Camera _cam;
    int _oldCoins;
    private object _oTweeenYoyo = new object();

    private void Awake()
    {
        _cam = Camera.main;
        _gm = FindObjectOfType<GameManager>();
        _gm.OnLevelSet += SetLevel;
        _gm.OnRepairDone += OnRepairDone;
        _rectTransform = transform as RectTransform;

        gameObject.SetActive(false);
    }

    private void SetLevel(LevelData levelData)
    {
        gameObject.SetActive(false);
        _oldCoins = _gm.Coins;
        _coins.text = $"{_oldCoins}";
    }

    private void OnRepairDone(Result result)
    {
        SoundManager.Instance.PlayCoins();
        if(!result.LevelData.IsBoss)_client.sprite = result.LevelData.Client;
        if (result.ClientHappiness == ClientHappiness.Happy)
        {
            SoundManager.Instance.PlayEffect(result.LevelData.ClientVoiceHappy);
        } else if (result.ClientHappiness == ClientHappiness.Sad) {
            SoundManager.Instance.PlayEffect(result.LevelData.ClientVoiceAngry);
        }
        _resultHappiness.sprite = result.LevelData.GetHappinessSprite(result.ClientHappiness);
        _resultHappiness.GetComponent<Animator>().runtimeAnimatorController = result.LevelData.GetHappinessAnim(result.ClientHappiness);

        _resultHappiness.GetComponent<Animator>().enabled = true;
        gameObject.SetActive(true);
        var h = _rectTransform.rect.height;
        _rectTransform.offsetMax = new Vector2(_rectTransform.offsetMax.x, h);
        _rectTransform.offsetMin = new Vector2(_rectTransform.offsetMin.x, h);

        var s = DOTween.Sequence()
            .Append(DOVirtual.Float(-10f, 0, 1f, val =>
            {
                _cam.transform.position = new Vector3(_cam.transform.position.x, val, _cam.transform.position.z);
            })
            .SetEase(Ease.InOutCubic))
            .Join(DOVirtual.Float(_rectTransform.rect.height, 0, 1f, val =>
            {
                _rectTransform.offsetMax = new Vector2(_rectTransform.offsetMin.x, val);
                _rectTransform.offsetMin = new Vector2(_rectTransform.offsetMin.x, val);
            })
            .SetEase(Ease.InOutCubic));

        if(result.GainedCoins > 0)
        {
            s.AppendInterval(0.2f);
            s.Append(DOVirtual.Float(_oldCoins, _oldCoins + result.GainedCoins, 1f, val =>
            {
                _coins.text = $"{(int)val}";
            }));
        }
            
        s.OnComplete(() =>
        {
            if(result.IsLastLevel)
            {
                _gm.ShowBossHappy(result.ClientHappiness == ClientHappiness.Happy);
            }
            else
            {
                NextLevel();
            }
        });        
    }

    private void NextLevel()
    {
        _client.transform.DOMoveY(.3f, .1f)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Yoyo)
            .SetId(_oTweeenYoyo);

        _client.transform.DOMoveX(-15, 2).SetEase(Ease.Linear).OnComplete(() =>
        {
            DOTween.Kill(_oTweeenYoyo);
            _gm.ShowNextLevel();
        });        
    }
}
