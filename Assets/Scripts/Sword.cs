﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Sword : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _mainSword;
    [SerializeField] private SpriteRenderer _temperatureSword;
    [SerializeField] private Gradient _temperatureRamp;

    GameManager _gm;
    LevelData _levelData;

    private float _currentBreakLevel;
    public float CurrentBreakLevel => _currentBreakLevel;
    public float OriginalBreakAmount => _levelData.BreakAmount;

    public GameObject goShines;

    private void Awake()
    {
        _gm = FindObjectOfType<GameManager>();
        _gm.OnLevelSet += SetLevel;
    }

    public void SetLevel(LevelData levelData)
    {
        _levelData = levelData;
        SetBreakLevel(_levelData.BreakAmount);
    }

    public void SetBreakLevel(float breakLevelBetweenZeroAndHundred)
    {
        Assert.IsNotNull(_levelData);
        _currentBreakLevel = breakLevelBetweenZeroAndHundred;
        _mainSword.sprite = _levelData.GetSpriteForBrokenLevel(breakLevelBetweenZeroAndHundred).Color;
        _temperatureSword.sprite = _levelData.GetSpriteForBrokenLevel(breakLevelBetweenZeroAndHundred).Mask;
        if (_currentBreakLevel <= 10)
        {
            goShines.SetActive(true);
        }
        else
        {
            goShines.SetActive(false);

        }
    }

    public void SetTemperature(float temperatureLevelBetweenZeroAndOne)
    {
        _temperatureSword.color = _temperatureRamp.Evaluate(temperatureLevelBetweenZeroAndOne);
    }
}
